FROM openjdk:8
WORKDIR /var/jenkins_home
COPY ./build/libs/test_master-0.0.1.jar hello.jar

CMD ["java", "-jar", "-Dspring.profiles.active=local", "hello.jar"]

EXPOSE 5000